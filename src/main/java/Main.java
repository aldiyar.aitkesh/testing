import domain.Owner;
import repositories.entities.OwnerRepository;
import repositories.interfaces.IEntityRepository;

import java.sql.*;

public class Main {
    public static void main(String[] args) {
//        try {
//            String connectionString = "jdbc:postgresql://localhost:5432/myapp";
//            Connection connection = DriverManager.getConnection(connectionString, "postgres", "admin1");
//            Statement stmt = connection.createStatement();
//            ResultSet rs = stmt.executeQuery("select * from owners");
//            while (rs.next()) {
//                System.out.println(rs.getString("name") + " " +  rs.getString("surname"));
//            }
//
//        } catch (SQLException ex){
//            System.out.println("Error: " + ex.getMessage());
//        }
        IEntityRepository repo = new OwnerRepository();
        Owner owner = new Owner("ELdos", "SSarkenov");
        repo.add(owner);
    }
}
