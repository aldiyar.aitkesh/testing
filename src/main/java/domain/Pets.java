package domain;
// singles
public class Pets extends Animals {
    private String breed;
    private int age;
    private String nickname;

    public Pets(String regid, String type, String breed, int age, String nickname) {
        super(regid, type);
        setAge(age);
        setBreed(breed);
        setNickname(nickname);
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return super.toString() + breed + "" + age + "" + nickname;
    }
}
