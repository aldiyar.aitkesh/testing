package domain;
// it is a superclass of herd and pets
public class Animals {
    private int id;
    private static int id_gen = 1;
    private String regid;
    private String type;



    public Animals(String regid, String type) {
        generateId();
        setRegid(regid);
        setType(type);
    }



    private void generateId() {
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public String getRegid(){
        return regid;
    }

    public String getType() {
        return type;
    }

    public void setRegid(String regid) {
        this.regid = regid;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return id + " " + regid + " " + type;
    }
}
