package domain;
//Forests and gardens
public class Plants {
    private int id;
    private static int id_gen = 1;
    private String regid;
    private String location;
    private String type;
    private double area;


    public Plants(String regid, String location, String type, double area) {
        generateId();
        setRegid(regid);
        setLocation(location);
        setType(type);
        setArea(area);
    }

    private void generateId() {
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public String getRegid() {
        return regid;
    }

    public void setRegid(String regid) {
        this.regid = regid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return id + " " + regid + " " + location + " " + type + " " + area;
    }
}
