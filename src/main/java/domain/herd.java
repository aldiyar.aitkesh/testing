package domain;

public class herd extends Animals {
    private long amount;

    public herd(String regid, String type, long amount) {
        super(regid, type);
        setAmount(amount);
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return super.toString() + amount;
    }
}
