package domain;

import java.security.Permission;

public class Owner {
    private int id = -1;
    private String name;
    private String surname;

    private Owner() {

    }

    public Owner(String name, String surname){
        setName(name);
        setSurname(surname);
    }

    public Owner(int id, String name, String surname){
        setId(id);
        setName(name);
        setSurname(surname);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName(){
        return name;
    }

    public String getSurname(){
        return surname;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setSurname(String surName) {
        this.surname = surName;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname;
    }


}
