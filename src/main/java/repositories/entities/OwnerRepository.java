package repositories.entities;

import domain.Owner;
import repositories.db.PostgresRepository;
import repositories.interfaces.IDBrepository;
import repositories.interfaces.IEntityRepository;

import java.sql.SQLException;
import java.sql.Statement;

public class OwnerRepository implements IEntityRepository<Owner> {
        private IDBrepository dbrepo;

        public OwnerRepository() {
            dbrepo = new PostgresRepository();
        }


        @Override
        public void add(Owner entity) {
            try {
                Statement stmt = dbrepo.getConnection().createStatement();
                String sql = "INSERT INTO owners(name, surname)" +
                        "VALUES ('" + entity.getName() + "', '" +
                        entity.getSurname() + "')";
                stmt.execute(sql);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

        @Override
        public void update(Owner owner) {

        }

        @Override
        public void remove(Owner owner) {

        }

        @Override
        public Iterable<Owner> query(String sql) {
            return null;
        }
    }
