package repositories.db;

import repositories.interfaces.IDBrepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class PostgresRepository implements IDBrepository {

    @Override
    public Connection getConnection() {
        try {
            String connectionString = "jdbc:postgresql://localhost:5432/myapp";
            Connection connection = DriverManager.getConnection(connectionString, "postgres", "admin1");
            Statement stmt = connection.createStatement();
            return connection;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
