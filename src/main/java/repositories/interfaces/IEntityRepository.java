package repositories.interfaces;

import java.util.ArrayList;

public interface IEntityRepository<T> {
    void add(T owner);
    void update(T owner);
    void remove(T owner);
    Iterable<T> query(String sql);

}
